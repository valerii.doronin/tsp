package com.knu.sequence;

import com.knu.data.City;
import com.knu.data.Path;
import com.knu.data.ResultDto;
import lombok.RequiredArgsConstructor;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@RequiredArgsConstructor
public class Sequence {

    private final Map<Integer, City> cities;
    private Path optimalPath;

    public ResultDto findPath(int startCityKey) {
        if(startCityKey < 0 || startCityKey > cities.keySet().size()) {
            throw new RuntimeException("Invalid start city index");
        }
        long start = System.nanoTime();
        ArrayList<Integer> availablePath = new ArrayList<>(cities.keySet());
        processNextCity(startCityKey, availablePath, new Path());
        long elapsedTime = TimeUnit.SECONDS.convert(System.nanoTime() - start, TimeUnit.NANOSECONDS);
        return buildResultDto(optimalPath, elapsedTime);
    }

    public void processNextCity(int key, List<Integer> availablePaths, Path currentPath) {
        List<Integer> paths = new ArrayList<>(availablePaths);
        paths.remove((Object) key);
        currentPath.addCity(cities.get(key), key);
        for (Integer pathKey : paths) {
            Path newPath = new Path(currentPath);
            processNextCity(pathKey, paths, newPath);
        }
        if(paths.isEmpty()){
            setOptimal(currentPath);
        }
    }

    private void setOptimal(Path path) {
        if(this.optimalPath == null){
            this.optimalPath = path;
        } else if (path.getDistance() < this.optimalPath.getDistance()){
            this.optimalPath = path;
        }
    }

    private ResultDto buildResultDto(Path path, long seconds){
        List<City> citiesPath = path.getPath().stream().map(cities::get).collect(Collectors.toList());
        return new ResultDto(citiesPath, seconds, path.getDistance());
    }

}
