package com.knu.parallel;

import com.knu.data.City;
import com.knu.data.ParallelDto;
import com.knu.data.Path;
import com.knu.data.ResultDto;
import com.knu.spr.ApplicationContextProvider;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.apache.catalina.core.ApplicationContext;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.context.support.StaticApplicationContext;
import org.springframework.core.env.Environment;
import org.springframework.web.client.RestTemplate;

import java.util.*;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import java.util.stream.Collectors;

@RequiredArgsConstructor
public class Parallel {

    private final Map<Integer, City> cities;
    private final ThreadPoolExecutor executor;
    @Value("#{'${server.queue}'.split(',')}")
    private Queue<String> urls;

    private Path optimalPath;
    private RestTemplate rest = new RestTemplate();

    public Parallel(Map<Integer, City> cities, int threadNumber) {
        Environment env = ApplicationContextProvider.getApplicationContext().getBean(Environment.class);
        urls = new LinkedList<>(Arrays.asList(env.getProperty("server.queue").split(",")));
        this.cities = cities;
        executor = (ThreadPoolExecutor) Executors.newFixedThreadPool(threadNumber);
    }

    public ResultDto findPath(int startCityKey) {
        if (startCityKey < 0 || startCityKey > cities.keySet().size()) {
            throw new RuntimeException("Invalid start city index");
        }
        long start = System.nanoTime();
        ArrayList<Integer> availablePath = new ArrayList<>(cities.keySet());
        Path p = new Path();
        p.addCity(cities.get(startCityKey), startCityKey);
        processNextCity(startCityKey, availablePath, p);
        executor.shutdown();
        try {
            executor.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
        } catch (InterruptedException e) {
            executor.shutdownNow();
        }
        long elapsedTime = TimeUnit.SECONDS.convert(System.nanoTime() - start, TimeUnit.NANOSECONDS);
        return buildResultDto(optimalPath, elapsedTime);
    }

    public void processNextCity(int key, List<Integer> availablePaths, Path path) {
        ArrayList<Integer> paths = new ArrayList<>(availablePaths);
        paths.remove((Object) key);
        for (Integer pathKey : paths) {
            submitThread(pathKey, paths, path);
        }
    }

    @SneakyThrows
    private void submitThread(int key, List<Integer> availablePaths, Path path) {
        Map<Integer, City> cities = availablePaths.stream().collect(Collectors.toMap(Function.identity(), this.cities::get));
        executor.submit(() -> {
            ParallelDto params = new ParallelDto(cities, path);
            Path p = rest
                    .postForObject(getUrl() + (key + 1), params, Path.class);
            this.setOptimal(p);
        });
    }

    private synchronized void setOptimal(Path path) {
        if (this.optimalPath == null) {
            this.optimalPath = path;
        } else if (path.getDistance() < this.optimalPath.getDistance()) {
            this.optimalPath = path;
        }
    }

    private synchronized String getUrl() {
        String url = this.urls.poll();
        this.urls.add(url);
        return url + "parallelThread/";
    }

    private ResultDto buildResultDto(Path path, long seconds) {
        List<City> citiesPath = path.getPath().stream().map(cities::get).collect(Collectors.toList());
        return new ResultDto(citiesPath, seconds, path.getDistance());
    }

}
