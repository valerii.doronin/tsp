package com.knu.data;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class ResultDto {

    private List<City> cities;
    private long seconds;
    private double distance;

}
