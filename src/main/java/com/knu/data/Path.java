package com.knu.data;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Data
@NoArgsConstructor
public class Path implements Serializable {

    private double distance;
    private List<Integer> path = new ArrayList<>();
    private City currentCity;

    public Path(Path path) {
        this.distance = path.getDistance();
        this.currentCity = path.getCurrentCity();
        this.path = new ArrayList<>(path.getPath());
    }

    public void addCity(City city, int key) {
        if(!path.isEmpty()) {
            this.distance += countDistance(city);
        }
        currentCity = city;
        path.add(key);
    }

    public void addCityForward(Map<Integer, City> cities, int key) {
        if(!path.isEmpty()) {
            City startCity = cities.get(key);
            City headCity = cities.get(path.get(0));
            this.distance += Math.sqrt(Math.pow(startCity.getX() - headCity.getX(), 2) + Math.pow(startCity.getY() - headCity.getY(), 2));
            path.add(0, key);
        } else {
            path.add(key);
        }
    }

    public double countDistance(City city) {
        return Math.sqrt(Math.pow(currentCity.getX() - city.getX(), 2) + Math.pow(currentCity.getY() - city.getY(), 2));
    }

    @Override
    public String toString() {
        return "Path{" +
                "path=" + path +
                '}';
    }
}
