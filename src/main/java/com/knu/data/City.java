package com.knu.data;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class City {

    private double x;
    private double y;

    @Override
    public String toString() {
        return "[" + x + ";" + y + "]";
    }
}
