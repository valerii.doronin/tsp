package com.knu.controller;

import com.knu.data.City;
import com.knu.data.ParallelDto;
import com.knu.data.Path;
import com.knu.data.ResultDto;
import com.knu.parallel.Parallel;
import com.knu.sequence.Sequence;
import com.knu.thread.Executor;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@RestController
public class Controller {

    @PostMapping("/sequence/{startIndex}")
    @ResponseBody
    public ResultDto sequence(@RequestBody List<City> cities, @PathVariable int startIndex) {
        Sequence s = new Sequence(IntStream.range(0, cities.size())
                .boxed()
                .collect(Collectors.toMap(Function.identity(), cities::get)));
        return s.findPath(startIndex - 1);
    }

    @PostMapping("/thread/{startIndex}")
    @ResponseBody
    public ResultDto thread(@RequestBody List<City> cities, @PathVariable int startIndex) {
        Path p = new Path();
        p.addCity(cities.get(startIndex - 1), startIndex - 1);
        Executor e = new Executor(IntStream.range(0, cities.size())
                .boxed()
                .collect(Collectors.toMap(Function.identity(), cities::get)), 12);
        return e.findPath(startIndex - 1, p);
    }

    @PostMapping("/parallelThread/{startIndex}")
    @ResponseBody
    public Path parallelThread(@RequestBody ParallelDto params, @PathVariable int startIndex) {
        params.getPath().addCity(params.getCities().get(startIndex - 1), startIndex - 1);
        Executor e = new Executor(params.getCities(), 12);
        return e.process(startIndex - 1, params.getPath());
    }

    @PostMapping("/parallel/{startIndex}")
    @ResponseBody
    public ResultDto parallel(@RequestBody List<City> cities, @PathVariable int startIndex) {
        Parallel e = new Parallel(IntStream.range(0, cities.size())
                .boxed()
                .collect(Collectors.toMap(Function.identity(), cities::get)), 3);
        return e.findPath(startIndex - 1);
    }
}
