package com.knu.thread;

import com.knu.data.City;
import com.knu.data.Path;
import com.knu.sequence.Sequence;
import lombok.RequiredArgsConstructor;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

@RequiredArgsConstructor
public class Multithreading {

    private final Map<Integer, City> cities;
    private Path optimalPath;

    public Path findPath(int startCityKey, List<Integer> availablePath, Path path) {
        processNextCity(startCityKey, availablePath, new Path(path));
        return optimalPath;
    }

    public void processNextCity(int key, List<Integer> availablePaths, Path currentPath) {
        List<Integer> paths = new ArrayList<>(availablePaths);
        paths.remove((Object) key);
        currentPath.addCity(cities.get(key), key);
        for (Integer pathKey : paths) {
            Path newPath = new Path(currentPath);
            processNextCity(pathKey, paths, newPath);
        }
        if(paths.isEmpty()){
            setOptimal(currentPath);
        }
    }

    private void setOptimal(Path path) {
        if(this.optimalPath == null){
            this.optimalPath = path;
        } else if (path.getDistance() < this.optimalPath.getDistance()){
            this.optimalPath = path;
        }
    }

}