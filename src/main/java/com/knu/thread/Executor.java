package com.knu.thread;

import com.knu.data.City;
import com.knu.data.Path;
import com.knu.data.ResultDto;
import com.knu.sequence.Sequence;
import lombok.SneakyThrows;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

public class Executor {

    private final Map<Integer, City> cities;
    private final ThreadPoolExecutor executor;

    private Path optimalPath;

    public Executor(Map<Integer, City> cities, int threadNumber) {
        this.cities = cities;
        executor = (ThreadPoolExecutor) Executors.newFixedThreadPool(threadNumber);
    }

    public ResultDto findPath(int startCityKey, Path p) {
        if(startCityKey < 0 || startCityKey > cities.keySet().size()) {
            throw new RuntimeException("Invalid start city index");
        }
        long start = System.nanoTime();
        process(startCityKey, p);
        long elapsedTime = TimeUnit.SECONDS.convert(System.nanoTime() - start, TimeUnit.NANOSECONDS);
        return buildResultDto(optimalPath, elapsedTime);
    }

    public Path process(int startCityKey, Path p) {
        if(startCityKey < 0 || startCityKey > cities.keySet().size()) {
            throw new RuntimeException("Invalid start city index");
        }
        ArrayList<Integer> availablePath = new ArrayList<>(cities.keySet());
        processNextCity(startCityKey, availablePath, p);
        executor.shutdown();
        try {
            executor.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
        } catch (InterruptedException e) {
            executor.shutdownNow();
        }
        return optimalPath;
    }

    public void processNextCity(int key, List<Integer> availablePaths, Path path) {
        ArrayList<Integer> paths = new ArrayList<>(availablePaths);
        paths.remove((Object) key);
        for (Integer pathKey : paths) {
            submitThread(pathKey, paths, path);
        }
    }

    @SneakyThrows
    private void submitThread(int key, List<Integer> availablePaths, Path path) {
        executor.submit(() -> {
            Path p = new Multithreading(cities).findPath(key, availablePaths, path);
            this.setOptimal(p);
        });
    }

    private synchronized void setOptimal(Path path) {
        if(this.optimalPath == null){
            this.optimalPath = path;
        } else if (path.getDistance() < this.optimalPath.getDistance()){
            this.optimalPath = path;
        }
    }

    private ResultDto buildResultDto(Path path, long seconds){
        List<City> citiesPath = path.getPath().stream().map(cities::get).collect(Collectors.toList());
        return new ResultDto(citiesPath, seconds, path.getDistance());
    }

}
